export const dones = [{
  "_id": "636975ac7287ab5b43482470",
  "name": "escucha activa",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3226904864312, -1.98216234959986] },
  "users": [{ "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }, { "_id": "6369758d7287ab5b434817e3", "name": "Mnarin", "username": "Mnarin_8vq111vqg", "email": "_yyjdune0n@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@mamen" }], "center": "43.3115926521147,-1.98697502502292", "__v": 0 }, { "_id": "6369758d7287ab5b434817ca", "name": "Txurruka", "username": "Txurruka_wn52pfh9z", "email": "_j7urn9jt9@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@txurruka" }, { "type": "tfno", "data": 609484413 }], "center": "43.3076859093751,-2.38724959418086", "__v": 0 }, { "_id": "6369758d7287ab5b434817cc", "name": "Isabeli", "username": "Isabeli_0k8dup4tz", "email": "_belfjs0sm@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Tierraestella" }], "center": "43.324179838593,-1.97692908005009", "__v": 0 }, { "_id": "6369758d7287ab5b434817ce", "name": "Maite", "username": "Maite_8q24cqp0m", "email": "_1kco8zk76@gplaza.eus", "contact_ways": [{ "type": "tfno", "data": 650431859 }], "center": "43.324179838593,-1.97692908005009", "__v": 0 }, { "_id": "6369758d7287ab5b434817d8", "name": "Amanda", "username": "Amanda_r1rvv9t9a", "email": "_s5gnxux1r@gplaza.eus", "contact_ways": [{ "type": "tfno", "data": 650202940 }], "center": "43.3076859093751,-2.38724959418086", "__v": 0 }, { "_id": "6369758d7287ab5b434817e0", "name": "Mertxe Larrañaga", "username": "Mertxe Larrañaga_kssf1trrt", "email": "_sm5mqxtmz@gplaza.eus", "contact_ways": [{ "type": "email", "data": "mertxetz@hotmail.com" }, { "type": "tfno", "data": 685714659 }], "center": "43.3206430882313,-1.95233080545868", "__v": 0 }],

  "metadata": { "_id": "636975aa7287ab5b434822b9", "necesidades": 0, "dones": 7, "descriptions": ["Don-descripción39", "Don-descripción77", "Don-descripción247", "Don-descripción255", "Don-descripción261", "Don-descripción282", "Don-descripción292"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b43482479",
  "name": "monitor tiempo libre niños",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3226904864312, -1.98216234959986] },
  "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }, { "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b434822a1", "necesidades": 0, "dones": 2, "descriptions": ["Don-descripción13", "Don-descripción37"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b43482480",
  "name": "preparar clases de meditación",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3226904864312, -1.98216234959986] },
  "users": [{ "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b434822bb", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción41"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b4348247c",
  "name": "ayuda DiY",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315b6973160bf2ee3a516", "name": "lo" }, { "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3296303030584, -1.82064853255169] },
  "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b434822a0", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción12"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b43482494",
  "name": "(des)carga mercados",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3172119368173, -1.97544130496173] },
  "users": [{ "_id": "6369758d7287ab5b43481796", "name": "Egoitz", "username": "Egoitz_jtf5qfadv", "email": "_3u9nvvtfj@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@egoitz" }], "center": "43.3172119368173,-1.97544130496173", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b43482296", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción2"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b4348248f",
  "name": "ayuda TICs (básico)",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3226904864312, -1.98216234959986] },
  "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }, { "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b4348229b", "necesidades": 0, "dones": 2, "descriptions": ["Don-descripción7", "Don-descripción28"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b4348248d",
  "name": "formación profesores lenguas extranjeras",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3226904864312, -1.98216234959986] },
  "users": [{ "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b434822b7", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción36"], "__v": 0 }, "__v": 0
}
  ,
{
  "_id": "636975ac7287ab5b4348248a",
  "name": "diseño 3D Solidworks",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3296303030584, -1.82064853255169] },
  "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b4348229f", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción11"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b43482481",
  "name": "traslado personas (recorridos cortos)",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3296303030584, -1.82064853255169] },
  "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }, { "_id": "6369758d7287ab5b434817e5", "name": "Marijose", "username": "Marijose_horzqp1fv", "email": "_rk0zxhnf1@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Marijose20" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b4348229d", "necesidades": 0, "dones": 2, "descriptions": ["Don-descripción9", "Don-descripción125"], "__v": 0 }, "__v": 0
},
{
  "_id": "636975ac7287ab5b43482487",
  "name": "ayuda TICs",
  "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
  "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }],
  "location": { "type": "Point", "coordinates": [43.3172119368173, -1.97544130496173] },
  "users": [{ "_id": "6369758d7287ab5b43481796", "name": "Egoitz", "username": "Egoitz_jtf5qfadv", "email": "_3u9nvvtfj@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@egoitz" }], "center": "43.3172119368173,-1.97544130496173", "__v": 0 }],
  "metadata": { "_id": "636975aa7287ab5b43482298", "necesidades": 0, "dones": 1, "descriptions": ["Don-descripción4"], "__v": 0 }, "__v": 0
}]
