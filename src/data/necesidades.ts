export const necesidades = [{
    "_id": "636975bb7287ab5b43482b2b",
    "name": "Aprender plantas medicinales",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3222903119054, -1.95233080545868] },
    "users": [{ "_id": "6369758d7287ab5b4348179c", "name": "Ane", "username": "Ane_xb8x07jvq", "email": "_p5x9votsp@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Ane_Sol" }], "center": "43.3226904864312,-1.98216234959986", "__v": 0 }, { "_id": "6369758d7287ab5b434817b0", "name": "Jaione", "username": "Jaione_vioki2jc9", "email": "_ethi9g8hk@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Jaione_C" }], "center": "43.3222903119054,-1.95233080545868", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a03", "necesidades": 3, "dones": 0, "descriptions": ["Necesidad-descripción27", "Necesidad-descripción92", "Necesidad-descripción93"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b22",
    "name": "Aprender dieta saludable",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315ae973160bf2ee3a3b0", "name": "jan" }, { "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3172119368173, -1.97544130496173] },
    "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b434829ec", "necesidades": 1, "dones": 0, "descriptions": ["Necesidad-descripción3"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b1d",
    "name": "Aprender a pescar",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3172119368173, -1.97544130496173] },
    "users": [{ "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b434829ee", "necesidades": 1, "dones": 0, "descriptions": ["Necesidad-descripción5"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b34",
    "name": "Batukada",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3166643322062, -2.01213007459821] },
    "users": [{ "_id": "6369758d7287ab5b434817ae", "name": "Eva", "username": "Eva_o9a1py04z", "email": "_sdmpwt49z@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@eva" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a30", "necesidades": 1, "dones": 0, "descriptions": ["Necesidad-descripción75"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b4d",
    "name": "Osteopatía",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3166643322062, -2.01213007459821] },
    "users": [{ "_id": "6369758d7287ab5b434817aa", "name": "Xabi", "username": "Xabi_rstohqdzg", "email": "_y6pcmw465@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@XabiKalbi" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }, { "_id": "6369758d7287ab5b434817ae", "name": "Eva", "username": "Eva_o9a1py04z", "email": "_sdmpwt49z@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@eva" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a2b", "necesidades": 2, "dones": 0, "descriptions": ["Necesidad-descripción70", "Necesidad-descripción77"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b70",
    "name": "Escucha",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3166643322062, -2.01213007459821] },
    "users": [{ "_id": "6369758d7287ab5b434817e5", "name": "Marijose", "username": "Marijose_horzqp1fv", "email": "_rk0zxhnf1@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Marijose20" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }, { "_id": "6369758d7287ab5b434817cc", "name": "Isabeli", "username": "Isabeli_0k8dup4tz", "email": "_belfjs0sm@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Tierraestella" }], "center": "43.324179838593,-1.97692908005009", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a37", "necesidades": 2, "dones": 2, "descriptions": ["Necesidad-descripción84", "Necesidad-descripción156"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b64",
    "name": "Aprender a arreglar bici",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3115926521147, -1.98697502502292] },
    "users": [{ "_id": "6369758d7287ab5b434817e3", "name": "Mnarin", "username": "Mnarin_8vq111vqg", "email": "_yyjdune0n@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@mamen" }], "center": "43.3115926521147,-1.98697502502292", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a1e", "necesidades": 1, "dones": 0, "descriptions": ["Necesidad-descripción57"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b5d",
    "name": "Aprender carpinteria",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3172119368173, -1.97544130496173] },
    "users": [{ "_id": "6369758d7287ab5b43481796", "name": "Egoitz", "username": "Egoitz_jtf5qfadv", "email": "_3u9nvvtfj@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@egoitz" }], "center": "43.3172119368173,-1.97544130496173", "__v": 0 }, { "_id": "6369758d7287ab5b434817b0", "name": "Jaione", "username": "Jaione_vioki2jc9", "email": "_ethi9g8hk@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Jaione_C" }], "center": "43.3222903119054,-1.95233080545868", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b434829eb", "necesidades": 2, "dones": 0, "descriptions": ["Necesidad-descripción2", "Necesidad-descripción89"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b6e",
    "name": "Trabajos fontanería",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315b6973160bf2ee3a516", "name": "lo" }], "location": { "type": "Point", "coordinates": [43.324179838593, -1.97692908005009] },
    "users": [{ "_id": "6369758d7287ab5b434817e5", "name": "Marijose", "username": "Marijose_horzqp1fv", "email": "_rk0zxhnf1@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Marijose20" }], "center": "43.3166643322062,-2.01213007459821", "__v": 0 }, { "_id": "6369758d7287ab5b434817be", "name": "Angel", "username": "Angel_34sw04c83", "email": "_xxudpj8pm@gplaza.eus", "contact_ways": [{ "type": "tfno", "data": 640025725 }], "center": "43.3190879726024,-2.01610018621829", "__v": 0 }, { "_id": "6369758d7287ab5b434817cc", "name": "Isabeli", "username": "Isabeli_0k8dup4tz", "email": "_belfjs0sm@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Tierraestella" }], "center": "43.324179838593,-1.97692908005009", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b43482a36", "necesidades": 3, "dones": 0, "descriptions": ["Necesidad-descripción83", "Necesidad-descripción133", "Necesidad-descripción154"], "__v": 0 }, "__v": 0
},
{
    "_id": "636975bb7287ab5b43482b50",
    "name": "Aprender inglés",
    "community": { "_id": "6369758d7287ab5b434817b4", "name": "Donosti", "center": "43.322181,-1.984496", "__v": 0 },
    "categories": [{ "_id": "633315a0973160bf2ee39fad", "name": "lagunak" }], "location": { "type": "Point", "coordinates": [43.3076859093751, -2.8724959418086] },
    "users": [{ "_id": "6369758d7287ab5b43481796", "name": "Egoitz", "username": "Egoitz_jtf5qfadv", "email": "_3u9nvvtfj@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@egoitz" }], "center": "43.3172119368173,-1.97544130496173", "__v": 0 }, { "_id": "6369758d7287ab5b43481798", "name": "Garikoitz", "username": "Garikoitz_n4wmq0wvc", "email": "_l510ndal8@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@gari" }], "center": "43.3296303030584,-1.82064853255169", "__v": 0 }, { "_id": "6369758d7287ab5b434817ca", "name": "Txurruka", "username": "Txurruka_wn52pfh9z", "email": "_j7urn9jt9@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@txurruka" }, { "type": "tfno", "data": 609484413 }], "center": "43.3076859093751,-2.38724959418086", "__v": 0 }, { "_id": "6369758d7287ab5b434817cc", "name": "Isabeli", "username": "Isabeli_0k8dup4tz", "email": "_belfjs0sm@gplaza.eus", "contact_ways": [{ "type": "telegram", "data": "@Tierraestella" }], "center": "43.324179838593,-1.97692908005009", "__v": 0 }],
    "metadata": { "_id": "636975ba7287ab5b434829ea", "necesidades": 4, "dones": 0, "descriptions": ["Necesidad-descripción1", "Necesidad-descripción7", "Necesidad-descripción151", "Necesidad-descripción152"], "__v": 0 }, "__v": 0
}]